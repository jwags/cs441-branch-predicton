#Code by Jordan Wagner
#Fall 2015 - CS 441 - Homework 4 - Profesor William Confer
#SUNY POLY
import time
import sys
import re
#define functions
#Convert hex to binary
def HexToBin(Hex):
    return bin(int(Hex, 16))[2:].zfill(8)#temp.split("\t")[0]

#Fixed-True branch prediction:
#Always predicts that the branch will be taken
def FixedTrue(DataFile):
    Yes = 0
    No = 0
    for CurLine in DataFile:
        if CurLine.split("\t")[1] == '@':
            Yes = Yes + 1
        else:
            No = No + 1
    print("Fixed:",Yes,"/",(Yes+No))
    DataFile.seek(0)
    return

#Static-First branch prediction:
#For every new branching, predict true. If the branch is not new (has been attempted before)
#predict its first outcome. If a branch is replacing an old branch act like it is a new branch
#(predict true)
def StaticFirstBranch(DataFile):
    Yes = 0
    No = 0
    #History[TAG, Prediction, Initialized (dirty)]
    History = [["0"*39,True,False] for x in range(128)] 
    for CurLine in DataFile:
        Binary = str(HexToBin(CurLine.split("\t")[0][2:]))
        Slot = int(Binary[-7:],2)
        Tag = Binary[:-8]
        if History[Slot][2] == False:
            if CurLine.split("\t")[1] == '@':
                Yes = Yes + 1
                History[Slot][1] = True
            else:
                No = No + 1
                History[Slot][1] = False
            History[Slot][2] = True
            History[Slot][0] = Tag
        elif History[Slot][2] == True and History[Slot][0] != Tag:
            if CurLine.split("\t")[1] == '@':
                Yes = Yes + 1
                History[Slot][1] = True
            else:
                No = No + 1
                History[Slot][1] = False
            History[Slot][0] = Tag
        elif History[Slot][2] == True and History[Slot][0] == Tag:
            if CurLine.split("\t")[1] == '@' and History[Slot][1]==True:
                Yes = Yes + 1
            elif CurLine.split("\t")[1] == '@' and History[Slot][1]==False:
                No = No + 1
            elif CurLine.split("\t")[1] == '.' and History[Slot][1]==True:
                No = No + 1
            elif CurLine.split("\t")[1] == '.' and History[Slot][1]==False:
                Yes = Yes + 1
        else:
            print("We can never reach this point!!!")
    print("Static:",Yes,"/",(Yes+No))
    DataFile.seek(0)
    return

#Two-Layer Adaptive branch prediction
#Predictions are first initialized to True. After that they are based on the last n branches and
#on the record of the last s occurences of the particular record of the last n branches
def TwoLayerAdaptive(DataFile):
    Yes = 0
    No = 0
    #History[TAG, Prediction=[[mispredictionCount,Prediction]*4], 2Ago, 1Ago]
    History = [["0"*39,[[0,True] for y in range(4)],False,False] for x in range(128)]
    for CurLine in DataFile:
        Binary = str(HexToBin(CurLine.split("\t")[0][2:]))
        Slot = int(Binary[-7:],2)
        Tag = Binary[:-8]
        if History[Slot][2] == False and History[Slot][3] == False:
            ago = 0
        elif History[Slot][2] == False and History[Slot][3] == True:
            ago = 1
        elif History[Slot][2] == True and History[Slot][3] == False:
            ago = 2
        elif History[Slot][2] == True and History[Slot][3] == True:
            ago = 3
        else:
            print("I just hate when we go nowhere")
        if History[Slot][0] != Tag:
            History[Slot][1] = [[0,True]]*4
            History[Slot][2] = False
            History[Slot][3] = False
        if History[Slot][1][ago][0] == 0 and History[Slot][1][ago][1] == True:
            if CurLine.split("\t")[1] == '@':
                Yes = Yes + 1
            else:
                No= No + 1
                History[Slot][1][ago][0] = 1
        elif History[Slot][1][ago][0] == 0 and History[Slot][1][ago][1] == False:
            if CurLine.split("\t")[1] == '.':
                Yes = Yes + 1
            else:
                No= No + 1
                History[Slot][1][ago][0] = 1
        elif History[Slot][1][ago][0] == 1 and History[Slot][1][ago][1] == True:
            if CurLine.split("\t")[1] == '@':
                Yes = Yes + 1
                History[Slot][1][ago][0] = 0
            else:
                No= No + 1
                History[Slot][1][ago][0] = 0
                History[Slot][1][ago][1] = False
        elif History[Slot][1][ago][0] == 1 and History[Slot][1][ago][1] == False:
            if CurLine.split("\t")[1] == '.':
                Yes = Yes + 1
                History[Slot][1][ago][0] = 0
            else:
                No= No + 1
                History[Slot][1][ago][0] = 0
                History[Slot][1][ago][1] = True
        else:
            print("We reached a spot that it is not okay to be at 0.0")
        History[Slot][0] = Tag
        History[Slot][2] = History[Slot][3]
        if CurLine.split("\t")[1] == '@':
            History[Slot][3] = True
        else:
            History[Slot][3] = False
    print("Dynamic:",Yes,"/",(Yes+No))
    DataFile.seek(0)
    return

#main
#Open Files
DataFile = open(sys.argv[1], 'r')
#execute Branch Predictions
FixedTrue(DataFile)
StaticFirstBranch(DataFile)
TwoLayerAdaptive(DataFile)
DataFile.close()